const http = require('http');


//Mock Data
let items = [

	{
		name: "Iphone X",
		price: 30000,
		isActive: true
	},
	{
		name: "Samsung Galaxy S21",
		price: 51000,
		isActive: true
	},
	{
		name: "Razer Blackshark VX2",
		price: 2800,
		isActive: false
	}
]
/*
	Activity:
	
	>> Create a new http server using node.js http module assign it to port 8000

	>> Create 2 new routes with both GET and POST methods

	First route is on "/items" endpoint and it is a GET method request.
		-Status code: 200
		-Headers: Content-Type: application/json
		-End the response with end() and send the items array as a JSON to our postman client.

	2nd route is on "/items" endpoint and it is a POST method request.
		-Receive the request body from our Postman client with our 2 step functions data step and end step.
		-Simulate the creation of a new item document and add the new object into the items array
		-End the response with end() inside the req.on('end') and send the updated items array as JSON into our client.
		-Use the post method route in our index.js for user creation as reference.
		-Status code: 200
		-Headers: Content-Type: application/json

		Stretch Goal:

		Add 1 route for the endpoint "/items" as DELETE method
		
	 	This route should be able delete the last item in the items array. Send the updated items array in the client as json.

*/


	http.createServer((req, res) => {

		// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
		// The method "GET" means that we will be retrieving or reading information
		if(req.url === '/' && req.method === 'GET'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking GET method')
		
		} else if(req.url === '/' && req.method === 'POST'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking POST method')
		
		} else if(req.url === '/' && req.method === 'PUT'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking PUT method')

		} else if(req.url === '/' && req.method === 'DELETE'){

			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking DELETE method')
		
		} else if(req.url === '/items' && req.method === 'GET'){

			// Change the value of Content-Type header if we are passing json as our server response: 'application/json'
			res.writeHead(200, {'Content-Type' : 'application/json'})

			// We cannot pass other data type as a response except for strings
			// To be able to pass the array of items, first we stringify the array as JSON
			res.end(JSON.stringify(items));
		
		} else if(req.url === '/items' && req.method === 'POST'){

			// Declare and intialize a "requestBody" variable to an empty string
			// This will act as a placeholder for the resource/data to be created later on
			let requestBody = '';

			// Receiving data from client to nodejs server requires 2 steps:

			// data step- this part will read the stream of data from our client and process the incoming data into the requestBody variable
			req.on('data', (data) => {

				console.log(data);

				// Assigns the data retrieved from the data stream to requestBody
				requestBody += data;
			});

			// end step- will run once or after the request has been completely sent from our client
			req.on('end', () => {

				console.log(requestBody);

				requestBody = JSON.parse(requestBody);

				// Create a new object representing the new mock database record
				let newItem = {
					name: requestBody.name,
					price: requestBody.price,
					isActive: requestBody.isActive
				}


				// Add the new item into the mock database
				items.push(newItem);
				console.log(items);

				res.writeHead(200, {'Content-Type' : 'application/json'})
				res.end(JSON.stringify(items))
			})

			}

	}).listen(8000)
	console.log('Server is running at port 8000');